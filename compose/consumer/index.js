const Koa = require('koa'),
      app = new Koa(),
      router = require('koa-router')(),
      koaBody = require('koa-body');

const NATS = require('nats'),
      nc = NATS.connect({
        url: 'nats://nats:4222',
        preserveBuffers: true
      });

const inbox = [];

nc.subscribe("comm", (msg) => {
  inbox.push(msg.toString());
});


router.get('/', (ctx) => {
    // console.log(ctx.request.body);
    // => POST body
    ctx.body = {
      items: inbox
    };
  }
);

app.use(router.routes());

app.listen(3000);
console.log('Consumer running!');
